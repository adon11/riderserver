var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var mongodb = require('mongodb');
var bodyParser = require('body-parser');
var request = require('request');
var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : 'password',
  database : 'HACKATHON'
});



var app = express();

//MIDDLEWARE
app.use(bodyParser.json());

//ROUTES
app.get('/', function(req, res) {
	res.status(200).send({  "getMap" : "/getmap",
						    "getsettings" : "/settings/:username",
						    "new user" : "/login/init",
						    "auth user": '/login/auth'
						});
					});
	

var array = [];

app.post('/getmap', function(req, res) {
	/*
	 *   given end destination
	 *   get eeveryone and limit by state
	 * 	 find end destinations within 2 km of the final destination
	 *   from this group check every person in the group as a way point where acceptable is %10 of original time 
	 * 	 show this to the user 
	 * 	 Save this list of people who's start point is in acceptable limit
	 * 	 Send start points to map
	 * 	 
	 * 
	 */
	 //var state = req.body.state;
	 var state = 'Vic';
	 var streetNo = '4 third ave';
	 var suburb = 'Aspendale';
     var startstate = 'Vic';
	 var startstreetNo = '41 Stewart St';
	 var startsuburb = 'Richmond';
	 var pickupPointsArray = [];
	 var bestRouteTime = 0;
	 connection.query("Select * from Address where state = ? and Final = 1 order by AddressID" , state, function(err, result){
	 	//console.log(result);
	 	//result = JSON.parse(result);
	 	for(var i = 0; i < result.length; i++) {
	 		
 			(function(a){
 				var obj = result[a];
 				request("https://maps.googleapis.com/maps/api/distancematrix/json?origins="+obj.StreetNo+obj.Suburb+obj.State+"&destinations="+streetNo+suburb+state+"&key=AIzaSyBEUGheef-8ohN3yXtjNRo_csvRBmnWA-8", function(error, response, body) {
				  if (!error && response.statusCode == 200) {
				    var anobj = response.body; // Print the google web page results.
				    data = JSON.parse(anobj);
				    distance = data.rows[0].elements[0].distance.value;
				    //console.log(distance);
				    if (distance < 2000){ //Determine whether all destinations are within 2km of the end point
				    	 connection.query("Select * from Address where AddressID = ? and Final = 0" , obj.AddressID, function(err, row){
					    	 array.push(row);
					    	 //console.log(array);
					   	});	
				    }
				}
			  });
 			})(i);
 			
		}
		});
		
		
		var routePolyLine='';
		request("https://maps.googleapis.com/maps/api/directions/json?origin="+startstreetNo+startsuburb+startstate+"&destination="+streetNo+suburb+state+"&key=AIzaSyBEUGheef-8ohN3yXtjNRo_csvRBmnWA-8", function(error, response, body) {
				  if (!error && response.statusCode == 200) {
				    var gooobj = response.body; // Print the google api response
				    //console.log(gooobj);
				    data = JSON.parse(gooobj);
				    //console.log(data.routes[0].legs[0].duration.value);
				    bestRouteTime = data.routes[0].legs[0].duration.value;
				    //console.log(data.routes[0].overview_polyline.points);
				    routePolyLine = data.routes[0].overview_polyline.points;
				 }
				   
		
				   for (i=0; i< array.length; i++){
						(function(a){
							if (array[a][0] != undefined){
							//console.log('define array');
							request("https://maps.googleapis.com/maps/api/directions/json?origin="+startstreetNo+startsuburb+startstate+"&destination="+streetNo+suburb+state+"&waypoints=via:"+array[a][0].StreetNo+array[a][0].Suburb+"&key=AIzaSyBEUGheef-8ohN3yXtjNRo_csvRBmnWA-8", function(error, response, body) {
								var wayobj = response.body;
								//console.log('request is back');
								tripdata = JSON.parse(wayobj);
								tripTime = tripdata.routes[0].legs[0].duration.value;
								if (tripTime < bestRouteTime + (60*10)){
									connection.query("Select * from Users where UID = ?", array[a][0].AddressID, function(err, rows){
											pickupPointsArray.push(rows);
											console.log('thing happened' + rows[0]);
									});
									
								}
							});
							}
						})(i);	    

					}
			});
					
				
				
				
		
		
		//console.log({"polyline": routePolyLine});
});

//
app.get('/settings/:username/:id', function (req, res) {
	console.log(req.params.username);
	console.log(req.params.id);
	res.status(200).send({  "_id" : req.params.id,
						    "username" : req.params.username,
						    "email" : "cwbuecheler@nospam.com",
						    "Other ": 'thing'
						});
					});
					
// ADD USER				
app.post('/login/init', function(req, res){
	
	var password = req.body.password;
	var username = req.body.username;
	var email 	 = req.body.email;
	
	if (password == undefined || username == undefined || email==undefined){
		res.status(200).send({'Message': 'Invalid data',
								'Data': {"password":password,"username":username, "email":email}});
	}
	//ADD USER TO DATABASE
	//connection.query( 'Select max(UID) from Users;'));
	connection.query("Select max(UID) from Users;", function(err,rows){
        var max = rows[0]['max(UID)'] + 1;
        var newMax = max.toString();
        connection.query('Insert into users(UID, UserName, Email, UserPassword ) values(?,?,?,?);', [newMax, username , email, password], function(err, result){} );
       	//console.log('Insert into users(UID, UserName, Email, UserPassword ) values(?,?,?,?,?);', [max, username , email, password]);
        console.log('user addition worked');
        connection.end();
        res.status(200).send({'Message': 'Successfully added user', 'err': err});//, 'UID': max});
        });
	
	
});

// AUTHENTICATE USERS
//app.post('/login/auth', function(req, res){
	//check against database of users
	//connection.query('Select * from users where id=? and username=?',[id, username])
//});



app.listen(process.env.PORT || 80);